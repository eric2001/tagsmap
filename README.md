# TagsMap
![TagsMap](./screenshot.jpg) 

## Description
TagsMap is a module for Gallery 3 which will allow a Gallery administrator to assign GPS coordinates to tags.  Those tags can then be viewed by visitors on a google map, accessible from the sidebar.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "tagsmap" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu. Once activated, you will have and Admin -> Content -> TagsMap Settings option. Visit this screen to configure the module and to assign GPS coordinates to tags.

## History
**Version 3.0.0:**
> - Renamed "Google Maps API key" to "Google APIs Console Key".
> - Made Console Key optional.
> - Added option to restrict the map page to registered users.
> - Added breadcrumbs to the map page.
> - Increased module version number to 3.
> - Released 02 July 2012.
>
> Download: [Version 3.0.0](/uploads/cdae93483028e2c706f40143d7fb10cc/tagsmap300.zip)

**Version 2.0.2:**
> - Set default text and link colors for the Google map dialog's to ensure that it will be visible on both dark and light colored themes.
> - Released 15 December 2011.
>
> Download: [Version 2.0.2](/uploads/2f3d3df67492a29124f5bcefd07fcbd3/tagsmap202.zip)

**Version 2.0.1:**
> - Updated "this module requires the tags module" warning messages to current gallery standards.
> - Fully tested for use with Gallery 3.0.2.
> - Released 07 June 2011.
>
> Download: [Version 2.0.1](/uploads/45236e83e257d7eb0fc209aa9e770333/tagsmap201.zip)

**Version 2.0.0:**
> - Compatibility fixes for Gallery 3.0.1.
> - Released 26 April 2011.
>
> Download: [Version 2.0.0](/uploads/4f449d00a23757d75137d737cd045009/tagsmap200.zip)

**Version 1.3.0:**
> - Updated for recent API changes in current Gallery 3 Git.
> - Fixed issue with tag links not working that was caused by a recent API change.
> - Fixed issue with the "removed orphaned gps data" button deleting all GPS data, which was caused by the recent framework upgrade.
> - Fixed issue with the delete GPS data from a single tag button deleting all GPS data, which was caused by the recent framework upgrade.
> - Tested everything against Gallery 3 Git (as of commit 38f2784fbbb0661dc57627d2878cb640bbffe271).
> - Released 19 January 2010.
>
> Download: [Version 1.3.0](/uploads/e55eb88d6e37ca34d6f0be42ea8c7f06/tagsmap130.zip)

**Version 1.2.0:**
> - Merged in ckieffer's CSS changes for Gallery 3 Git
> - Merged in some of the changes present in the 3nids version of TagsMap.
> - Tested everything against current git (as of commit b6c1ba7ea6416630b2a44b3df8400a2d48460b0a)
> - Released 14 October 2009
>
> Download: [Version 1.2.0](/uploads/88411a7a17f6a1a42f20330be42f0f70/tagsmap120.zip)

**Version 1.1.2:**
> - Replace p::clean with html::clean for recent Gallery API change.
> - Released 31 August 2009
>
> Download: [Version 1.1.2](/uploads/62ccebda4e6df3a13fe9b8b48b06313f/tagsmap112.zip)

**Version 1.1.1:**
> - Firefox specific bug-fix.
> - Released 05 August 2009
>
> Download: [Version 1.1.1](/uploads/2f5b31a4fd95f11770545c1c1eb6d7a6/tagsmap111.zip)

**Version 1.1.0:**
> - Includes bharat's changes for the recent API update.
> - Released 31 July 2009
>
> Download: [Version 1.1.0](/uploads/77c7517b49da187f2bed28fc45113efe/tagsmap110.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 26 July 2009
>
> Download: [Version 1.0.0](/uploads/fa9fdf05b3a82603c257ab9bae8c7288/tagsmap100.zip)
